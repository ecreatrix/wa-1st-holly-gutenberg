<?php
/**
 * @wordpress-plugin
 * Plugin Name: 1st Holly Gutenberg blocks
 * Description: Gutenberg blocks for theme use
 * Version:     2022.4
 * Author:      Alyx
 * License:     GPL-2.0+
 * License URI: http:// www.gnu.org/licenses/gpl-2.0.txt
 */
namespace wa\Plugin\Gutenberg;

defined( __NAMESPACE__ . '\URI' ) || define( __NAMESPACE__ . '\URI', plugin_dir_url( __FILE__ ) );
defined( __NAMESPACE__ . '\PATH' ) || define( __NAMESPACE__ . '\PATH', plugin_dir_path( __FILE__ ) );

$files = [
  'app/Helpers',
  'app/Assets',
  'app/StockEdits',
  //'cpt-guestbook',
  //'public/blocks/container/index',
  'public/blocks/posts-preview/index',
];

foreach ( $files as $file ) {
  $file = plugin_dir_path( __FILE__ ) . $file . '.php';

  if ( file_exists( $file ) ) {
    require_once $file;
  } else {
    echo $file . ' not found.<br>';
  }
}