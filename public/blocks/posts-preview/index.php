<?php
namespace wa\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( PostsPreview::class ) ) {
	class PostsPreview {
		public function __construct() {
			add_action( 'init', [$this, 'register_block'], 20 );
			add_action( 'rest_api_init', [$this, 'register_route'] );
		}

		public static function get_author( $post_id ) {
			$name     = get_post_meta( $post_id, 'wa_author_name', true );
			$company  = get_post_meta( $post_id, 'wa_author_company', true );
			$position = get_post_meta( $post_id, 'wa_author_position', true );

			if ( $name && $company ) {
				$company = ' - ' . $company;
			}
			if ( $position && ( $name || $company ) ) {
				$position = ', ' . $position;
			}

			return $name . $company . $position;
		}

		public function get_filtered_post_types() {
			$types = get_post_types();

			$types_exclude = ['attachment', 'revision', 'nav_menu_item', 'custom_css', 'customize_changeset', 'oembed_cache', 'wp_block', 'wp_global_styles', 'user_request', 'wp_template', 'wp_template_part', 'acf-field-group', 'acf-field', 'popupbuilder', 'wp_navigation', 'page'];
			//$types_exclude = apply_filters( 'wa_post_block_types_exclude', $types_exclude );

			$types = \array_diff( $types, $types_exclude );
			//$types = apply_filters( 'wa_post_block_types', $types );

			return $types;
		}

		public function get_filtered_posts( $attributes, $show_all_posts ) {
			$order    = $attributes['order'];
			$ordering = preg_split( '#/#', $order );

			if ( 'random' === $ordering[0] ) {
				$ordering = ['order' => 'asc', 'orderBy' => 'random'];
			} else if ( count( $ordering ) != 2 ) {
				$ordering = ['order' => 'desc', 'orderBy' => 'title'];
			} else {
				$ordering = ['order' => $ordering[1], 'orderBy' => $ordering[0]];
			}

			$posts_per_page = -1;
			if ( $attributes['limitNumber'] ) {
				$posts_per_page = $attributes['postsToShow'];
			}

			$args = [
				'posts_per_page' => -1,
				'post_status'    => 'publish',
				'order'          => $ordering['order'],
				'orderby'        => $ordering['orderBy'],
				'post_type'      => $show_all_posts ? $this->get_filtered_post_types() : $attributes['postType'],

				//'suppress_filters' => false
			];

			if ( $attributes['showCategory'] ) {
				//$type  = $category[0]->slug;
				//$label = $label_name . ' - ' . $category[0]->name;
			}

			$recent_posts = get_posts( $args );

			return $recent_posts;
		}

		public function get_image_info( $post ) {
			$image = [
				'srcset'  => false,
				'src'     => false,
				'width'   => false,
				'height'  => false,
				'caption' => false,
			];

			if ( ! has_post_thumbnail( $post ) ) {
				return $image;
			}

			$thumbnail_id = get_post_thumbnail_id( $post );
			$thumbnail    = wp_get_attachment_image_src( $thumbnail_id, 'large' );

			if ( $thumbnail ) {
				$image = [
					'srcset'  => wp_get_attachment_image_srcset( $thumbnail_id ),
					'src'     => $thumbnail[0],
					'width'   => $thumbnail[1],
					'height'  => $thumbnail[2],
					'caption' => wp_get_attachment_caption( $thumbnail_id ),
				];
			}

			return $image;
		}

		// Get info with meta
		public function get_posts_by_type( \WP_REST_Request $request ) {
			$attributes = $request->get_params();
			$info       = $this->get_posts_info( $attributes, true );

			if ( $info ) {
				return new \WP_REST_Response(
					[
						'success'  => true,
						'response' => $info,
					],
					200
				);
			} else {
				return new \WP_REST_Response(
					[
						'error'      => true,
						'success'    => false,
						'error_code' => 'no_posts',
						'response'   => __( 'No ' . $request['type'] . ' posts were found.', 'wa-theme' ),
					],
					401
				);
			}
		}

		public function get_posts_info( $attributes, $show_all_posts = false ) {
			$posts = $this->get_filtered_posts( $attributes, $show_all_posts );

			$return = [];
			if ( $show_all_posts ) {
				$return = [];
			} else {
				$return = [
					'label'       => '',
					'archive'     => '',
					'posts'       => [],
					//'blocks'      => [],
					'compilation' => '',
					'categories'  => [],
				];
			}

			$categories = [];
			foreach ( $posts as $post ) {
				$id               = $post->ID;
				$type             = $post->post_type;
				$post_type_object = get_post_type_object( $type );

				$label_name = $post_type_object->labels->name;

				$categories[$type]['all'] = ['label' => 'All', 'value' => 'all'];
				$category                 = get_the_category( $id );
				$category_slug            = 'none';
				$caregory_label_text      = '&nbsp;';
				if ( is_array( $category ) && ! empty( $category ) ) {
					$category_slug                     = lcfirst( str_replace( '-', '', ucwords( $category[0]->slug, '-' ) ) ); // use camelCase instead of dash-case
					$caregory_label_text               = $category[0]->name;
					$categories[$type][$category_slug] = ['label' => $caregory_label_text, 'value' => $category_slug];
				}

				$classes = ['block post-' . $id . ' col-12'];
				if ( 4 == $attributes['columnsMobile'] ) {
					$classes[] = 'col-sm-3';
				} else if ( 3 == $attributes['columnsMobile'] ) {
					$classes[] = 'col-sm-4';
				} else if ( 2 == $attributes['columnsMobile'] ) {
					$classes[] = 'col-sm-6';
				}

				if ( 4 == $attributes['columnsDesktop'] ) {
					$classes[] = 'col-lg-3';
				} else if ( 3 == $attributes['columnsDesktop'] ) {
					$classes[] = 'col-lg-4';
				} else if ( 2 == $attributes['columnsDesktop'] ) {
					$classes[] = 'col-sm-6';
				}

				$category_label = '';
				if ( $attributes['showCategory'] && 'all' !== $attributes['showCategory'] ) {
					$category_label = $caregory_label_text;
					$classes[]      = 'has-category';
				}

				$meta_classes = ['meta'];
				$date         = false;
				$date_text    = get_the_date( '', $id );
				if ( $attributes['displayDate'] ) {
					$date           = $date_text;
					$meta_classes[] = 'has-date';
				}

				$author      = false;
				$author_text = get_the_author_meta( 'display_name', get_post_field( 'post_author', $id ) );
				if ( $attributes['displayAuthor'] ) {
					$author         = $author_text;
					$meta_classes[] = 'has-author';
				}

				$link = get_permalink( $id );

				$archive = false;
				if ( $attributes['displaySectionMoreButton'] ) {
					$archive   = $this->get_read_more( $type );
					$classes[] = 'has-more-button';
				}

				//$categories = get_the_category( $id );

				$image_info = $this->get_image_info( $post );
				if ( $attributes['displayImage'] && $image_info['src'] ) {
					$classes[] = 'has-image';
				}

				$title_text = get_the_title( $id );

				if ( post_password_required( $post ) ) {
					$post_content_text = __( 'This content is password protected.' );
					$post_excerpt_text = __( 'This content is password protected.' );
				} else {
					$post_content_text = get_the_content( null, false, $id );
					$post_excerpt_text = get_the_excerpt( $id );

					$content = '';
					if ( 'content' === $attributes['displayContent'] ) {
						$content   = $post_content_text;
						$classes[] = 'has-content';
					} else if ( 'excerpt' === $attributes['displayContent'] ) {
						$content   = $post_excerpt_text;
						$classes[] = 'has-excerpt';
					} else if ( $post_excerpt_text ) {
						$content   = $post_excerpt_text;
						$classes[] = 'has-excerpt';
					} else if ( $post_content_text ) {
						$content   = $post_content_text;
						$classes[] = 'has-content';
					}
				}

				$content_tag       = false;
				$post_content_text = $post_content_text;
				$post_excerpt_text = $post_excerpt_text;
				if ( $attributes['displayContent'] && 'none' != $attributes['displayContent'] ) {
					$content_tag = apply_filters( 'the_content', $content );

					if ( $attributes['limitLength'] ) {
						if ( $attributes['keepFormatting'] ) {
							$content_tag = force_balance_tags( html_entity_decode( wp_trim_words( htmlentities( $content_tag, $attributes['textLength'] ) ) ) );
						} else {
							$content_tag = wp_trim_words( $content_tag, $attributes['textLength'] );
						}
						//var_dump( $content_tag );
					}
				}

				$post = [
					'id'            => $id,
					'link'          => $link,
					'imageInfo'     => $image_info,
					'title'         => $title_text,
					'author'        => $author_text,
					'date'          => $date_text,
					'content'       => $content_tag,
					'postContent'   => $post_content_text,
					'postExcerpt'   => $post_excerpt_text,
					'category'      => $category_slug,
					'categoryLabel' => $category_label,
					'classes'       => implode( ' ', $classes ),
					'metaClasses'   => implode( ' ', $meta_classes ),
					//'extra'        => $extra,
					//'block'        => '<div class="block post-' . $id . ' col-12 col-lg-3 col-md-6 col-sm-12">' . $image . $content . $extra . '</div>',
				];

				if ( 'all' !== $attributes['showCategory'] && $category_slug === $attributes['showCategory'] ) {
					$label_name    = $category_label;
					$archive_label = $category_label;
				}

				$title = $label_name;
				//var_dump( $attributes['showCategory'] . ' ' . $category_label . ' ' . $caregory_label_text );
				//var_dump( $label_name . ' ' . $attributes['showCategory'] . ' ' . $category_label . ' ' . $category_slug );
				if ( 'location' !== $type ) {
					$title = 'Latest ' . $title;
				}

				if ( $show_all_posts ) {
					///$return['by_type'][$type]['label'] = $label_name;
					///$return['by_type'][$type]['title'] = $title;
					///$return['by_type'][$type]['type']  = $type;
					//$return['by_type'][$type]['archive_label'] = $archive_label;
					///$return['by_type'][$type]['archive'] = $archive;
					///$return['by_type'][$type]['posts'][] = $post;
					//$return['by_type'][$type]['blocks'][] = $post['block'];
				} else {
					//var_dump( $category_slug . ' ' . $attributes['showCategory'] );
					if ( 'all' !== $attributes['showCategory'] && $category_slug === $attributes['showCategory'] ) {
						$return['label'] = $label_name;
						$return['title'] = $title;
						$return['type']  = $type;
					} else if ( 'all' === $attributes['showCategory'] ) {
						$return['label'] = $label_name;
						$return['title'] = $title;
						$return['type']  = $type;
					}

					//$return['archive_label'] = $archive_label;
					$return['archive'] = $archive;
					$return['posts'][] = $post;
					//$return['blocks'][] = $post['block'];
				}
			}

			if ( $show_all_posts ) {
				foreach ( $return['by_type'] as $key => $info ) {
					$post_type_object = get_post_type_object( $key );

					$return['types'][] = [
						'label' => $post_type_object->labels->name,
						'value' => $key,
					];
					$return['categories'][$key] = $categories[$key];

					//$return['by_type'][$key]['compilation'] = $return['by_type'][$key]['label'] . '<div class="posts row">' . implode( '', $return['by_type'][$key]['blocks'] ) . '</div>' . $return['by_type'][$key]['archive'];
				}

				foreach ( $return['categories'] as $key => $info ) {
					$return['categories'][$key] = array_values( $info );
				}
			} else {
				//$return['categories_keyed'] = $categories;
				$return['categories'] = array_values( $categories );
				//$return['compilation'] = $return['label'] . '<div class="posts row">' . implode( '', $return['blocks'] ) . '</div>' . $return['archive'];
			}

			return $return;
		}

		public function get_read_more( $type ) {
			$blog_url = get_permalink( get_option( 'page_for_posts' ) );
			$url      = get_post_type_archive_link( $type );

			if ( 'post' === $type && $blog_url ) {
				// Blog posts need custom links
				$url = $blog_url;
			}

			return $url;
		}

		public function manage_options_permission() {
			if ( ! current_user_can( 'manage_options' ) ) {
				return new \WP_REST_Response(
					[
						'message'  => __( 'User doesn\'t have permissions to change options.', 'wa-theme' ),
						'type'     => 'error',
						'code'     => 'user_dont_have_permission',
						'returned' => false,
					],
					401
				);
			}

			return true;
		}

		public function register_block() {
			$block_json_file = BlockHelpers::block_json( 'posts-preview' );

			register_block_type( $block_json_file, [
				'render_callback' => [$this, 'render_callback'],
			] );
		}

		public function register_route() {
			register_rest_route(
				'wagt/v1',
				'/get_posts_by_type?(?P<attributes>[\w]+)',
				[
					'methods'             => \WP_REST_Server::READABLE,
					'callback'            => [$this, 'get_posts_by_type'],
					'permission_callback' => [$this, 'manage_options_permission'],
				]
			);
		}

		public function render_callback( $attributes, $content ) {
			$info = $this->get_posts_info( $attributes );

			$category_label = '';
			if ( $attributes['showCategory'] && 'all' !== $attributes['showCategory'] ) {
				//$category_label = '<span class="category">' . $info['categories_keyed'][$attributes['postType']][$attributes['showCategory']]['label'] . '</span>';
			}

			$label = '';
			if ( $attributes['displaySectionHeading'] ) {
				$label = '<h2 class="label">' . $info['title'] . '</h2>';
			}

			//var_dump( array_keys( $info ) );
			$archive = '';
			if ( $attributes['displaySectionMoreButton'] && $info['archive'] ) {
				$archive = '<div class="archive wa-buttons"><a class="btn btn-outline-primary" href="' . $info['archive'] . '">View all ';
				$archive .= '' !== $category_label ? $category_label : $info['label'];
				$archive .= '</a></div>';
			}

			$return = [];
			$count  = [
				'all' => 1,
			];
			//var_dump( $info['posts'] );

			foreach ( $info['posts'] as $i => $item ) {
				if ( $attributes['showCategory'] && 'all' !== $attributes['showCategory'] && $item['category'] !== $attributes['showCategory'] ) {
					continue;
				}
				$category = $item['category'];

				if ( $attributes['limitNumber'] ) {
					if ( ! array_key_exists( $category, $count ) ) {
						// Keep track of count by category
						$count[$category] = 1;
					}

					if ( $count[$category] > $attributes['postsToShow'] && $attributes['limitByCategory'] ) {
						// Skip post, category limit reached
						continue;
					} else if ( $attributes['limitByCategory'] ) {
						// Add post, category limit not reached
						$count[$category] = $count[$category] + 1;
					} else if ( $count['all'] > $attributes['postsToShow'] ) {
						// Skip post, total limit reached
						break;
					} else {
						// Add post, total limit not reached
						$count['all'] = $count['all'] + 1;
					}
				}

				$id = $item['id'];
				//var_dump( get_the_content( null, false, $id ) );

				$image = '';
				if ( $attributes['displayImage'] && $item['imageInfo']['src'] ) {
					$image = '<img class="attachment-full size-full wp-post-image" width="' . $item['imageInfo']['width'] . '" height="' . $item['imageInfo']['height'] . '" src="' . $item['imageInfo']['src'] . '" alt="" src-set="' . $item['imageInfo']['srcset'] . '" sizes="(max-width: 512px) 100vw, 512px" />';

					if ( $attributes['displayPostMoreButton'] ) {
						$image = '<a class="btn btn-link block-link" href=' . $item['link'] . '>' . $image . '</a>';
					}

					$image = '<div class="image">' . $image . '</div>';
				}

				$date = '';
				if ( $attributes['displayDate'] ) {
					$date = '<span class="date">' . $item['date'] . '</span>';
				}

				$author = '';
				if ( $attributes['displayAuthor'] ) {
					$author = '<span class="author">' . $item['author'] . '</span>';
				}

				$title = '<a class="btn btn-link stretched-link" href=' . $item['link'] . '><h4 class="title">' . $item['title'] . '</h4></a>';

				$extra = '<div class="bg"><div class="special"></div></div>';
				$extra = '';

				$read_more = '';
				if ( $attributes['displayPostMoreButton'] ) {
					$read_more = '<a class="btn btn-outline-primary read-more" href="' . $item['link'] . '">Read More</a>';
				}

				$post_category_label = '';
				if ( $attributes['showCategory'] && 'all' === $attributes['showCategory'] ) {
					$post_category_label = '<div class="category h4 mb-1">' . $item['categoryLabel'] . '</div>';
				}

				$meta    = '<div class="' . $item['metaClasses'] . '">' . $author . $date . '</div>';
				$intro   = '<div class="intro">' . $post_category_label . $image . $meta . $title . '</div>';
				$content = '<div class="text">' . $item['content'] . '</div>' . $extra;

				$return[] = '<div class="' . $item['classes'] . '"><div class="content position-relative">' . $intro . $content . '</div>' . $read_more . '</div>';
			}

			//var_dump( $this->get_filtered_post_types() );
			return '<div class="jumbotron wp-block-wa-posts-preview type-' . $info['type'] . '"><div class="container">' . $label . '<div class="posts row">' . implode( $return ) . '</div>' . $archive . '</div></div>';
		}
	}

	new PostsPreview();
}