<?php
namespace wa\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Container::class ) ) {
	class Container {
		public function __construct() {
			//add_action( 'init', [$this, 'register_block'], 20 );
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'container' );

			register_block_type( $block_json_file, [
				'render_callback' => 'render_callback'] );
		}

		function render_callback( $block_attributes, $content ) {
			return $content;
		}
	}

	new Container();
}