<?php
namespace wa\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( StockEdits::class ) ) {
	class StockEdits {
		public function __construct() {
			//add_filter( 'block_categories_all', [$this, 'filter_block_categories'], 10, 2 );
		}

		function filter_block_categories( $block_categories, $editor_context ) {
			unset( $block_categories['embeds'] );
			unset( $block_categories['widgets'] );

			return $block_categories;
		}
	}

	new BlockHelpers();
}